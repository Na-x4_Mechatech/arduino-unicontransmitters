#define UNICON_MAJOR_VERSION  1
#define UNICON_MINOR_VERSION  0
#define UNICON_PATCH_VERSION  0

//#define BAUDRATE        115200
#define BAUDRATE        114000
#define INTERVAL        50

#define NUMOF_BUTTONS   17
#define NUMOF_AXIS      20

uint8_t ledPin = 6;

#include <PS3USB.h>
#include <SPI.h>
USB usb;
PS3USB ps3(&usb);

typedef struct {
  AnalogHatEnum id;
  bool isInverted;
} AxisTableElement;

ButtonEnum buttonTable[] = {CIRCLE, CROSS, TRIANGLE, SQUARE,
                            UP, RIGHT, DOWN, LEFT,
                            L1, R1, L2, R2,
                            L3, R3, SELECT, START, PS
                           };
AxisTableElement axisTable[] = {{LeftHatX, false}, {LeftHatY, true}, {RightHatX, false}, {RightHatY, true}};
SensorEnum sensorTable[] = {aX, aY, aZ, gZ};

void setup() {
  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, LOW);

  Serial.begin(BAUDRATE);
  if (usb.Init() == -1) {
    while (1) {
      digitalWrite(ledPin, HIGH);
      delay(250);
      digitalWrite(ledPin, LOW);
      delay(250);
    }
  }

  digitalWrite(ledPin, HIGH);
}

void loop() {
  bool buttonsState[NUMOF_BUTTONS] = {};
  uint8_t axisValues[NUMOF_AXIS] = {};

  usb.Task();

  if (ps3.PS3Connected) {
    digitalWrite(ledPin, HIGH);
    for (int i = 0; i < NUMOF_BUTTONS; i++) {
      buttonsState[i] = ps3.getButtonPress(buttonTable[i]);
    }

    for (int i = 0; i < 4; i++) {
      if (!axisTable[i].isInverted) {
        axisValues[i] = ps3.getAnalogHat(axisTable[i].id);
      } else {
        axisValues[i] = 255 - ps3.getAnalogHat(axisTable[i].id);
      }
    }
    for (int i = 0; i < 4; i++) {
      axisValues[i + 4] = constrain(ps3.getSensor(sensorTable[i]) / 4, 0x00, 0xFF);
    }
    for (int i = 0; i < 12; i++) {
      axisValues[i + 8] = ps3.getAnalogButton(buttonTable[i]);
    }
  } else {
    digitalWrite(ledPin, LOW);
  }

  const size_t bufferSize = generateData(NULL, 0, buttonsState, NUMOF_BUTTONS, axisValues, NUMOF_AXIS);
  uint8_t buffer[bufferSize];
  generateData(buffer, bufferSize, buttonsState, NUMOF_BUTTONS, axisValues, NUMOF_AXIS);
  sendData(buffer, bufferSize);

  delay(INTERVAL);
}


size_t generateData(uint8_t buffer[], const size_t bufferSize, const bool buttonsState[], const int numofButtons, const uint8_t axisValues[], const int numofAxis) {
  const int buttonsOffset = 2;
  const int axisOffset = buttonsOffset + ((numofButtons + 7) / 8);
  const int checksumOffset = axisOffset + numofAxis;
  const size_t dataLength = checksumOffset + 1;

  if (bufferSize >= dataLength) {
    buffer[0] = numofButtons;
    buffer[1] = numofAxis;
    buttons2data(buffer + buttonsOffset, buttonsState, numofButtons);
    memcpy(buffer + axisOffset, axisValues, numofAxis);

    buffer[checksumOffset] = 0xFF;
    for (int i = 0; i < (dataLength - 1); i++) {
      buffer[checksumOffset] -= buffer[i];
    }
  }
  return dataLength;
}

void buttons2data(uint8_t data[], bool buttonsState[], int num) {
  for (int i = 0; i < num; i++) {
    if ((i % 8) == 0) {
      data[i / 8] = 0;
    }
    if (buttonsState[i]) {
      data[i / 8] |= 1 << (i % 8);
    }
  }
}

void sendData(uint8_t data[], size_t dataLength) {
  Serial.print("[");                // ヘッダ文字

  for (int i = 0; i < dataLength; i++) {
    if (i != 0) {
      Serial.print(".");            // 区切り文字
    }
    char temp[3];
    snprintf(temp, 3, "%02X", data[i]);
    Serial.print(temp);
  }

  Serial.print("]\n");              // フッタ文字
}

