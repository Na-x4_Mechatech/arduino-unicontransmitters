# DUALSHOCK3

DUALSHOCK3を使用する方法。

-----

## 目次

- [使用ライブラリ](#markdown-header-_1)
- [設定項目](#markdown-header-_2)
- [ピン割り当て](#markdown-header-_3)
- [受信側](#markdown-header-_4)
- [参考サイト](#markdown-header-_5)

## 使用ライブラリ
- [USB_Host_Shield_2.0][USB_Host_Shield_2.0] ライブラリ
    - ライセンス : [GPL-2.0][GPL-2.0]

[USB_Host_Shield_2.0]: https://github.com/felis/USB_Host_Shield_2.0
[GPL-2.0]: https://opensource.org/licenses/GPL-2.0

## 設定項目

行                                | 説明
--------------------------------- | --------------
`#define BAUDRATE        115200`  | ボーレート
`#define INTERVAL        50`      | 送信間隔 (ms)

## ピン割り当て

ピン番号   | 機能
---------- | ----------------------------
0          | XBee DOUT
1          | XBee DIN
6          | LED
7          | USB Host Shield Reset
8          | USB Host Shield GPX
9          | USB Host Shield INT
10         | USB Host Shield SS
11         | USB Host Shield MOSI
12         | USB Host Shield MISO
13         | USB Host Shield SCK

ピン番号は[SparkFun USB Host Shield][SparkFun/9947]([スイッチサイエンス][ssci/438])の[回路図][SparkFun/9947/Schematic]から引用した。
SparkFun USB Host Shieldを使用する際は[改造][ssci/438/wiki]が必要となるので注意。

[SparkFun/9947]: https://www.sparkfun.com/products/9947
[SparkFun/9947/Schematic]: https://www.sparkfun.com/datasheets/DevTools/Arduino/USBHostShield-v13.pdf
[ssci/438]: https://www.switch-science.com/catalog/438/
[ssci/438/wiki]: http://trac.switch-science.com/wiki/USBHostShield

## [受信側][mbed-UniController]

[mbed-UniController]: https://bitbucket.org/Na-x4_Mechatech/mbed-unicontroller

受信側での設定、各IDは以下の通り。

- ボタン数 : 17
- 軸数 : 20

### ボタンID

ID | 説明
-- | --------------------------
0  | Circle
1  | Cross
2  | Triangle
3  | Square
4  | 十字キー Up
5  | 十字キー Rigth
6  | 十字キー Down
7  | 十字キー Left
8  | L1
9  | R1
10 | L2
11 | R2
12 | L3
13 | R3
14 | Select
15 | Start
16 | Playstation Center Button

### 軸ID

ID | 説明
-- | --------------------------------
0  | 左アナログスティック X軸
1  | 左アナログスティック Y軸
2  | 右アナログスティック X軸
3  | 右アナログスティック Y軸
4  | 加速度センサ X軸
5  | 加速度センサ Y軸
6  | 加速度センサ Z軸
7  | 角速度センサ
8  | ボタン感圧センサ Circle
9  | ボタン感圧センサ Cross
10 | ボタン感圧センサ Triangle
11 | ボタン感圧センサ Square
12 | ボタン感圧センサ 十字キー Up
13 | ボタン感圧センサ 十字キー Rigth
14 | ボタン感圧センサ 十字キー Down
15 | ボタン感圧センサ 十字キー Left
16 | ボタン感圧センサ L1
17 | ボタン感圧センサ R1
18 | ボタン感圧センサ L2
19 | ボタン感圧センサ R2

## 参考サイト

- [felis/USB_Host_Shield_2.0/examples/PS3USB/PS3USB.ino · GitHub][USB_Host_Shield_2.0/PS3USB.ino]
- [USB Host Shield 2.0: PS3USB Class Reference][USB_Host_Shield_2.0/Classes/PS3USB]
- [DUALSHOCK3 入力リポート][TamamyIkesu/DS3report]
- ~~[ps3:hardware:sixaxis \[PS2Dev Wiki\]][PS2Wiki/SIXAXIS]~~ (リンク切れ)
    - [Wayback Machine][WArchive/PS2Wiki/SIXAXIS]から

[USB_Host_Shield_2.0/PS3USB.ino]: https://github.com/felis/USB_Host_Shield_2.0/blob/master/examples/PS3USB/PS3USB.ino
[USB_Host_Shield_2.0/Classes/PS3USB]: http://felis.github.io/USB_Host_Shield_2.0/class_p_s3_u_s_b.html
[TamamyIkesu/DS3report]: http://tamamyikesu.web.fc2.com/sd_reportdata.html
[PS2Wiki/SIXAXIS]: http://wiki.ps2dev.org:80/ps3:hardware:sixaxis
[WArchive/PS2Wiki/SIXAXIS]: https://web.archive.org/web/20130124175352/http://wiki.ps2dev.org:80/ps3:hardware:sixaxis
