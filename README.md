# Arduino-UniConTransmitters

高専ロボコン2017で富山本郷Aが使用したコントローラの送信側プログラム。
受信側プログラムは[こちら][mbed-UniController]。

[mbed-UniController]: https://bitbucket.org/Na-x4_Mechatech/mbed-unicontroller

--------

## 目次

- [導入方法](#markdown-header-_1)
- [ハードウェアについて](#markdown-header-_2)
- [スケッチについて](#markdown-header-_3)

## 導入方法

まず、このリポジトリを[ここ][master.zip]からダウンロードする。
次にArduino IDE を開き、`ファイル > 環境設定` から`スケッチブックの保存場所`を確認し、エクスプローラで開く。
ダウンロードした`master.zip`を`スケッチブックの保存場所`に展開する。Arduino IDE を再起動させて、`ファイル > スケッチブック`内にスケッチが増えていれば成功。

[master.zip]: https://bitbucket.org/Na-x4_Mechatech/Arduino-UniConTransmitters/get/master.zip

## ハードウェアについて

ATMEGA328Pを使用したものならおそらく動く。
無線モジュールはXBeeの透過モード相当のものであれば動作する。

動作確認済ハードウェア

- [Arduino Uno][ssci/789]
- [AE-ATMEGA-328 MINI][AKIZUKI/K-10347]
- チップ単体
    - [ここ][ArduinoOnBB]を参考にした。

[ssci/789]: https://www.switch-science.com/catalog/789/
[AKIZUKI/K-10347]: http://akizukidenshi.com/catalog/g/gK-10347/
[ArduinoOnBB]: https://www.arduino.cc/en/Tutorial/ArduinoToBreadboard

## スケッチについて

スケッチの詳細は各スケッチフォルダへ。

- [Standard](Standard/)
- [SNESController](SNESController/)
- [WiiNunchuck](WiiNunchuck/)
- [DUALSHOCK3](DUALSHOCK3/)
