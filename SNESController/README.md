# SNESController

スーパーファミコンのコントローラを使用する方法。

-----

## 目次

- [設定項目](#markdown-header-_1)
- [ピン割り当て](#markdown-header-_2)
- [受信側](#markdown-header-_3)
- [参考サイト](#markdown-header-_4)

## 設定項目

行                                | 説明
--------------------------------- | --------------
`#define BAUDRATE        115200`  | ボーレート
`#define INTERVAL        50`      | 送信間隔 (ms)

## ピン割り当て

ピン番号   | 機能
---------- | ----------------------------
0          | XBee DOUT
1          | XBee DIN
10         | SNES Data latch
12         | SNES Serial data
13         | SNES Data clock
A4         | LED

コントローラ側コネクタのピンアサインを[ここ][RepairFAQ/F_SNES]から引用する。
**ケーブルの色が違う場合があるのでコネクタ側を確認すること。**
>           ----------------------------- ---------------------
>          |                             |                      \
>          | (1)     (2)     (3)     (4) |   (5)     (6)     (7) |
>          |                             |                      /
>           ----------------------------- ---------------------
>     
>     
>            Pin     Description             Color of wire in cable
>            ===     ===========             ======================
>            1       +5v                     White
>            2       Data clock              Yellow
>            3       Data latch              Orange
>            4       Serial data             Red
>            5       ?                       no wire
>            6       ?                       no wire
>            7       Ground                  Brown

## [受信側][mbed-UniController]

[mbed-UniController]: https://bitbucket.org/Na-x4_Mechatech/mbed-unicontroller

受信側での設定、各IDは以下の通り。

- ボタン数 : 12
- 軸数 : 3

### ボタンID

ID | 説明
-- | -------------
0  | Aボタン
1  | Bボタン
2  | Xボタン
3  | Yボタン
4  | 十字ボタン上
5  | 十字ボタン右
6  | 十字ボタン下
7  | 十字ボタン左
8  | Lボタン
9  | Rボタン
10 | SELECTボタン
11 | STARTボタン

### 軸ID

ID | 説明
-- | -------------
0  | 十字ボタン左右
1  | 十字ボタン上下
2  | L/Rボタン

## 参考サイト

- [Super Nintendo Entertainment System: pinouts & protocol - Sci.Electronics.Repair FAQ][RepairFAQ/F_SNES]
- [スーパーファミコンコントローラの解析 - ハードウェアとか研究所][hwlabo/sfc_cont]
- [SFCコントローラの解析 - FAMILUNKER][FAMILUNKER/sfc_controller]

[RepairFAQ/F_SNES]: http://www.repairfaq.org/REPAIR/F_SNES.html
[hwlabo/sfc_cont]: https://sites.google.com/site/hardware007laboratory/home/denshi-kousaku/sfc_cont
[FAMILUNKER/sfc_controller]: http://familunker.web.fc2.com/electric/sfc_controller.html
