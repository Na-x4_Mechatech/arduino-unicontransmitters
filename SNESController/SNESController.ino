#include <SPI.h>

#define UNICON_MAJOR_VERSION  1
#define UNICON_MINOR_VERSION  0
#define UNICON_PATCH_VERSION  0

#define BAUDRATE        115200
#define INTERVAL        50

#define NUMOF_BUTTONS   12
#define NUMOF_AXIS      3

uint8_t ledPin = A4;

#define SNES_A  0
#define SNES_B  1
#define SNES_X  2
#define SNES_Y  3

#define SNES_UP     4
#define SNES_RIGHT  5
#define SNES_DOWN   6
#define SNES_LEFT   7

#define SNES_LEFT_TRIGGER   8
#define SNES_RIGHT_TRIGGER  9
#define SNES_SELECT 10
#define SNES_START  11

const uint8_t ssPin = 10;

void setup() {
  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, LOW);

  Serial.begin(BAUDRATE);
  initSNES();

  digitalWrite(ledPin, HIGH);
}

void loop() {
  bool buttonsState[NUMOF_BUTTONS];
  uint8_t axisValues[NUMOF_AXIS];

  readButton(buttonsState, NUMOF_BUTTONS);

  axisValues[0] = button2axis(buttonsState[SNES_RIGHT], buttonsState[SNES_LEFT]);
  axisValues[1] = button2axis(buttonsState[SNES_UP], buttonsState[SNES_DOWN]);
  axisValues[2] = button2axis(buttonsState[SNES_RIGHT_TRIGGER], buttonsState[SNES_LEFT_TRIGGER]);

  const size_t bufferSize = generateData(NULL, 0, buttonsState, NUMOF_BUTTONS, axisValues, NUMOF_AXIS);
  uint8_t buffer[bufferSize];
  generateData(buffer, bufferSize, buttonsState, NUMOF_BUTTONS, axisValues, NUMOF_AXIS);
  sendData(buffer, bufferSize);

  delay(INTERVAL);
}

void readButton(bool buttonsState[], int num) {
  uint16_t raw = 0;
  int buttonIdTable[12] = {SNES_RIGHT_TRIGGER, SNES_LEFT_TRIGGER,
                           SNES_X, SNES_A,
                           SNES_RIGHT, SNES_LEFT, SNES_DOWN, SNES_UP,
                           SNES_START, SNES_SELECT,
                           SNES_Y, SNES_B
                          };

  digitalWrite(ssPin, LOW);
  raw = SPI.transfer(0) << 8;
  raw |= SPI.transfer(0);
  digitalWrite(ssPin, HIGH);

  if (num >= 12) {
    raw >>= 4;
    for (int i = 0; i < 12; i++) {
      buttonsState[buttonIdTable[i]] = ((raw >> i) & 1) ? false : true;
    }
  }
}

uint8_t button2axis(bool a, bool b) {
  if (a && !b) {
    return UINT8_MAX;
  } else if (!a && b) {
    return 0;
  } else {
    return UINT8_MAX / 2;
  }
}

size_t generateData(uint8_t buffer[], const size_t bufferSize, const bool buttonsState[], const int numofButtons, const uint8_t axisValues[], const int numofAxis) {
  const int buttonsOffset = 2;
  const int axisOffset = buttonsOffset + ((numofButtons + 7) / 8);
  const int checksumOffset = axisOffset + numofAxis;
  const size_t dataLength = checksumOffset + 1;

  if (bufferSize >= dataLength) {
    buffer[0] = numofButtons;
    buffer[1] = numofAxis;
    buttons2data(buffer + buttonsOffset, buttonsState, numofButtons);
    memcpy(buffer + axisOffset, axisValues, numofAxis);

    buffer[checksumOffset] = 0xFF;
    for (int i = 0; i < (dataLength - 1); i++) {
      buffer[checksumOffset] -= buffer[i];
    }
  }
  return dataLength;
}

void buttons2data(uint8_t data[], bool buttonsState[], int num) {
  for (int i = 0; i < num; i++) {
    if ((i % 8) == 0) {
      data[i / 8] = 0;
    }
    if (buttonsState[i]) {
      data[i / 8] |= 1 << (i % 8);
    }
  }
}

void sendData(uint8_t data[], size_t dataLength) {
  Serial.print("[");                // ヘッダ文字

  for (int i = 0; i < dataLength; i++) {
    if (i != 0) {
      Serial.print(".");            // 区切り文字
    }
    char temp[3];
    snprintf(temp, 3, "%02X", data[i]);
    Serial.print(temp);
  }

  Serial.print("]\n");              // フッタ文字
}

void initSNES() {
  SPI.begin();
  SPI.setBitOrder(MSBFIRST);
  SPI.setDataMode(SPI_MODE2);
  pinMode(ssPin, OUTPUT);
  digitalWrite(ssPin, HIGH);
}

