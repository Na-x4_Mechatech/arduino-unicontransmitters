#define UNICON_MAJOR_VERSION  1
#define UNICON_MINOR_VERSION  0
#define UNICON_PATCH_VERSION  0

#define BAUDRATE        115200
#define INTERVAL        10

#define NUMOF_BUTTONS   11
#define NUMOF_AXIS      4

uint8_t buttonPins[NUMOF_BUTTONS];
uint8_t axisPins[NUMOF_AXIS];

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);

  Serial.begin(BAUDRATE);

  for (int i = 0; i < NUMOF_BUTTONS; i++) {
    buttonPins[i] = i + 2;
    pinMode(buttonPins[i], INPUT_PULLUP);
  }
  for (int i = 0; i < NUMOF_AXIS; i++) {
    axisPins[i] = i + A0;
    pinMode(axisPins[i], INPUT);
  }

  digitalWrite(LED_BUILTIN, HIGH);
}

void loop() {
  bool buttonsState[NUMOF_BUTTONS];
  uint8_t axisValues[NUMOF_AXIS];

  for (int i = 0; i < NUMOF_BUTTONS; i++) {
    buttonsState[i] = !digitalRead(buttonPins[i]);
  }
  for (int i = 0; i < NUMOF_AXIS; i++) {
    axisValues[i] = analogRead(axisPins[i]) / 4;
  }

  const size_t bufferSize = generateData(NULL, 0, buttonsState, NUMOF_BUTTONS, axisValues, NUMOF_AXIS);
  uint8_t buffer[bufferSize];
  generateData(buffer, bufferSize, buttonsState, NUMOF_BUTTONS, axisValues, NUMOF_AXIS);
  sendData(buffer, bufferSize);

  delay(INTERVAL);
}

size_t generateData(uint8_t buffer[], const size_t bufferSize, const bool buttonsState[], const int numofButtons, const uint8_t axisValues[], const int numofAxis) {
  const int buttonsOffset = 2;
  const int axisOffset = buttonsOffset + ((numofButtons + 7) / 8);
  const int checksumOffset = axisOffset + numofAxis;
  const size_t dataLength = checksumOffset + 1;

  if (bufferSize >= dataLength) {
    buffer[0] = numofButtons;
    buffer[1] = numofAxis;
    buttons2data(buffer + buttonsOffset, buttonsState, numofButtons);
    memcpy(buffer + axisOffset, axisValues, numofAxis);

    buffer[checksumOffset] = 0xFF;
    for (int i = 0; i < (dataLength - 1); i++) {
      buffer[checksumOffset] -= buffer[i];
    }
  }
  return dataLength;
}

void buttons2data(uint8_t data[], bool buttonsState[], int num) {
  for (int i = 0; i < num; i++) {
    if ((i % 8) == 0) {
      data[i / 8] = 0;
    }
    if (buttonsState[i]) {
      data[i / 8] |= 1 << (i % 8);
    }
  }
}

void sendData(uint8_t data[], size_t dataLength) {
  Serial.print("[");                // ヘッダ文字

  for (int i = 0; i < dataLength; i++) {
    if (i != 0) {
      Serial.print(".");            // 区切り文字
    }
    char temp[3];
    snprintf(temp, 3, "%02X", data[i]);
    Serial.print(temp);
  }

  Serial.print("]\n");              // フッタ文字
}

