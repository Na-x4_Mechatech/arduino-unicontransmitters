# WiiNunchuck

2つのWiiのヌンチャクを使用する方法。

-----

## 目次

- [使用ライブラリ](#markdown-header-_1)
- [設定項目](#markdown-header-_2)
- [ピン割り当て](#markdown-header-_3)
- [受信側](#markdown-header-_4)
- [参考サイト](#markdown-header-_5)

## 使用ライブラリ

- [SoftI2CMaster][SoftI2CMaster] ライブラリ
    - ライセンス : [GPL-3.0][GPL-3.0]
    - 2個目のヌンチャクを接続するために使用
- [WiiNunchuck](../libraries/WiiNunchuck/) ライブラリ
    - 自作

[SoftI2CMaster]: https://github.com/felias-fogg/SoftI2CMaster/
[GPL-3.0]: https://opensource.org/licenses/GPL-3.0

## 設定項目

行                                | 説明
--------------------------------- | --------------
`#define BAUDRATE        115200`  | ボーレート
`#define INTERVAL        50`      | 送信間隔 (ms)
`#define SCL_PIN 7`               | Nunchuck1のSCLピンの指定 詳細は下
`#define SCL_PORT PORTB`          | Nunchuck1のSCLピンの指定 詳細は下
`#define SDA_PIN 6`               | Nunchuck1のSDAピンの指定 詳細は下
`#define SDA_PORT PORTB`          | Nunchuck1のSDAピンの指定 詳細は下

SoftI2CMasterライブラリでは直接レジスタを叩いているためピン指定が特殊な形式となっている。
`SCL_PORT`、`SDA_PORT`にそれぞれAVRのポートを指定し、`SCL_PIN`、`SDA_PIN`でピン番号を指定する。
Arduinoのピン番号とAVRのポート、ピン番号の対応については「[Arduino - PortManipulation](https://www.arduino.cc/en/Reference/PortManipulation)」が詳しい。

## ピン割り当て

ピン番号   | 機能
---------- | ----------------------------
0          | XBee DOUT
1          | XBee DIN
2          | Nunchuck0 Detect
3          | Nunchuck1 Detect
13         | LED
A4         | Nunchuck0 SDA
A5         | Nunchuck0 SCL
PB6        | Nunchuck1 SDA
PB7        | Nunchuck1 SCL

**PB6、PB7ピンは内蔵発振に設定したATMEGA328Pのみ使用可能のため、他のArduinoで使用する場合には設定変更が必要。**

ヌンチャクとの接続には[WiiChuck Adapter][todbot/wiichuck]([スイッチサイエンス][ssci/221])を使用すると簡単だが、
接続検出用のピンが引き出されていないので延長ケーブルを切断したものを使用した。
Wii拡張コントローラのピンアサインは[ここ][WiiBrew/Connector]を参照。

[todbot/wiichuck]: http://todbot.com/blog/2008/02/18/wiichuck-wii-nunchuck-adapter-available/
[ssci/221]: https://www.switch-science.com/catalog/221/
[WiiBrew/Connector]: http://wiibrew.org/wiki/File:Wii_Connector.jpg

## [受信側][mbed-UniController]

[mbed-UniController]: https://bitbucket.org/Na-x4_Mechatech/mbed-unicontroller

受信側での設定、各IDは以下の通り。

- ボタン数 : 4
- 軸数 : 10

### ボタンID

ID | 説明
-- | ------------------
0  | Nunchuck0 Cボタン
1  | Nunchuck0 Zボタン
2  | Nunchuck1 Cボタン
3  | Nunchuck1 Zボタン

### 軸ID

ID | 説明
-- | -------------------------
0  | Nunchuck0 アナログスティック X軸
1  | Nunchuck0 アナログスティック Y軸
2  | Nunchuck0 加速度センサ X軸
3  | Nunchuck0 加速度センサ Y軸
4  | Nunchuck0 加速度センサ Z軸
5  | Nunchuck1 アナログスティック X軸
6  | Nunchuck1 アナログスティック Y軸
7  | Nunchuck1 加速度センサ X軸
8  | Nunchuck1 加速度センサ Y軸
9  | Nunchuck1 加速度センサ Z軸

## 参考サイト

- [Wiimote/Extension Controllers - WiiBrew][WiiBrew/XCtrlrs]
- [Wiimote/Extension Controllers/Nunchuck - WiiBrew][WiiBrew/XCtrlrs/Nunchuck]

[WiiBrew/XCtrlrs]: http://wiibrew.org/wiki/Wiimote/Extension_Controllers
[WiiBrew/XCtrlrs/Nunchuck]: http://wiibrew.org/wiki/Wiimote/Extension_Controllers/Nunchuck
