#define UNICON_MAJOR_VERSION  1
#define UNICON_MINOR_VERSION  0
#define UNICON_PATCH_VERSION  0

#define BAUDRATE        115200
#define INTERVAL        50

#define NUMOF_BUTTONS   4
#define NUMOF_AXIS      10

uint8_t ledPin = 13;

#include <Wire.h>

#define SCL_PIN 7
#define SCL_PORT PORTB
#define SDA_PIN 6
#define SDA_PORT PORTB
#include <SoftWire.h>

#include <WiiNunchuck.h>
#include <WireWiiNunchuck.h>

SoftWire SoftwareWire;

WireWiiNunchuck nunchuck0(Wire);
bool nunchuck0NeedsInitialize = true;

WireLikeWiiNunchuck<SoftWire> nunchuck1(SoftwareWire);
bool nunchuck1NeedsInitialize = true;

WiiNunchuck *nunchucks[2] = {&nunchuck0, &nunchuck1};

void setup() {
  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, LOW);

  Serial.begin(BAUDRATE);

  attachInterrupt(digitalPinToInterrupt(2), nunchuck0DetectInterrupt, RISING);
  attachInterrupt(digitalPinToInterrupt(3), nunchuck1DetectInterrupt, RISING);

  digitalWrite(ledPin, HIGH);
}

void loop() {
  bool buttonsState[NUMOF_BUTTONS];
  uint8_t axisValues[NUMOF_AXIS];

  nunchuckEvent(nunchuck0, nunchuck0NeedsInitialize);
  nunchuckEvent(nunchuck1, nunchuck1NeedsInitialize);

  for (int j = 0; j < 2; j++) {
    for (int i = 0; i < 2; i++) {
      axisValues[j * 2 + i] = constrain(map(nunchucks[j]->getStick(i), 0x20, 0xE0, 0x00, 0xFF), 0x00, 0xFF);
    }
    for (int i = 0; i < 3; i++) {
      axisValues[j * 3 + i + 4] = nunchucks[j]->getAccel(i) / 4;
    }
    for (int i = 0; i < 2; i++) {
      buttonsState[j * 2 + i] = nunchucks[j]->getButton(i);
    }
  }

  const size_t bufferSize = generateData(NULL, 0, buttonsState, NUMOF_BUTTONS, axisValues, NUMOF_AXIS);
  uint8_t buffer[bufferSize];
  generateData(buffer, bufferSize, buttonsState, NUMOF_BUTTONS, axisValues, NUMOF_AXIS);
  sendData(buffer, bufferSize);

  delay(INTERVAL);
}

void nunchuck0DetectInterrupt() {
  nunchuck0NeedsInitialize = true;
}

void nunchuck1DetectInterrupt() {
  nunchuck1NeedsInitialize = true;
}

void nunchuckEvent(WiiNunchuck &nunchuck, bool &needsInitialize) {
  digitalWrite(ledPin, LOW);
  if (needsInitialize) {
    delay(50);
    nunchuck.initialize();
    if (nunchuck.isConnected()) {
      needsInitialize = false;
    }
  }

  if (nunchuck.isConnected()) {
    nunchuck.requestData();
    /*
      for (int i = 0; i < 2; i++) {
      Serial.print(nunchuck.getStick(i));
      Serial.print(", ");
      }
      for (int i = 0; i < 3; i++) {
      Serial.print(nunchuck.getAccel(i));
      Serial.print(", ");
      }
      for (int i = 0; i < 2; i++) {
      Serial.print(nunchuck.getButton(i));
      }
      Serial.println();
    */
  } else {
    //Serial.println("Not Connected");
  }
  digitalWrite(ledPin, HIGH);
}

size_t generateData(uint8_t buffer[], const size_t bufferSize, const bool buttonsState[], const int numofButtons, const uint8_t axisValues[], const int numofAxis) {
  const int buttonsOffset = 2;
  const int axisOffset = buttonsOffset + ((numofButtons + 7) / 8);
  const int checksumOffset = axisOffset + numofAxis;
  const size_t dataLength = checksumOffset + 1;

  if (bufferSize >= dataLength) {
    buffer[0] = numofButtons;
    buffer[1] = numofAxis;
    buttons2data(buffer + buttonsOffset, buttonsState, numofButtons);
    memcpy(buffer + axisOffset, axisValues, numofAxis);

    buffer[checksumOffset] = 0xFF;
    for (int i = 0; i < (dataLength - 1); i++) {
      buffer[checksumOffset] -= buffer[i];
    }
  }
  return dataLength;
}

void buttons2data(uint8_t data[], bool buttonsState[], int num) {
  for (int i = 0; i < num; i++) {
    if ((i % 8) == 0) {
      data[i / 8] = 0;
    }
    if (buttonsState[i]) {
      data[i / 8] |= 1 << (i % 8);
    }
  }
}

void sendData(uint8_t data[], size_t dataLength) {
  Serial.print("[");                // ヘッダ文字

  for (int i = 0; i < dataLength; i++) {
    if (i != 0) {
      Serial.print(".");            // 区切り文字
    }
    char temp[3];
    snprintf(temp, 3, "%02X", data[i]);
    Serial.print(temp);
  }

  Serial.print("]\n");              // フッタ文字
}

