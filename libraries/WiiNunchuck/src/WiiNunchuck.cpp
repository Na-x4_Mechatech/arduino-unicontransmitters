#include "WiiNunchuck.h"

WiiNunchuck::WiiNunchuck()
  : _isConnected(false) {
}

void WiiNunchuck::initialize() {
  _write(0xF0, 0x55);
  _write(0xFB, 0x00);
  _checkIdentifier();
}

void WiiNunchuck::requestData() {
  uint8_t data[6];
  if (_readAndCheck(0x00, data, 6)) {
    _splitData(data);
  }
}

bool WiiNunchuck::isConnected() {
  return _isConnected;
}

int WiiNunchuck::getStick(AxisId id) {
  return _stick[id];
}

int WiiNunchuck::getAccel(AxisId id) {
  return _accel[id];
}

int WiiNunchuck::getButton(ButtonId id) {
  return _button[id];
}

bool WiiNunchuck::_readAndCheck(uint8_t address, uint8_t data[], int length) {
  if (_read(address, data, length) == length) {
    _isConnected = true;
    return true;
  } else {
    _isConnected = false;
    return false;
  }
}

void WiiNunchuck::_splitData(const uint8_t data[]) {
  _stick[XAxis] = data[0];
  _stick[YAxis] = data[1];
  _accel[XAxis] = (data[2] << 2) | ((data[5] & 0x0C) >> 2);
  _accel[YAxis] = (data[3] << 2) | ((data[5] & 0x30) >> 4);
  _accel[ZAxis] = (data[4] << 2) | ((data[5] & 0xC0) >> 6);
  _button[CButton] = !(data[5] & 0x02);
  _button[ZButton] = !(data[5] & 0x01);
}

bool WiiNunchuck::_checkIdentifier() {
  uint8_t id[6];
  return _readAndCheck(0xFA, id, 6);
}

