#ifndef WIINUNCHUCK
#define WIINUNCHUCK

#include <stdint.h>

class WiiNunchuck {
  public:
    WiiNunchuck();
    void initialize();
    void requestData();

    bool isConnected();

    enum AxisId {
      XAxis, YAxis, ZAxis
    };
    int getStick(AxisId);
    int getAccel(AxisId);

    enum ButtonId {
      CButton, ZButton
    };
    int getButton(ButtonId);

  protected:
    virtual int _read(uint8_t address, uint8_t data[], int length) = 0;
    virtual void _write(uint8_t address, uint8_t data) = 0;

  private:
    int _stick[2];
    int _accel[3];
    bool _button[2];

    bool _isConnected;

    bool _readAndCheck(uint8_t address, uint8_t data[], int length);
    void _splitData(const uint8_t []);
    bool _checkIdentifier();
};

#endif // WIINUNCHUCK

