#ifndef WIREWIINUNCHUCK
#define WIREWIINUNCHUCK

#include "WiiNunchuck.h"

#include <Wire.h>

template<class WireLikeT>
class WireLikeWiiNunchuck : public WiiNunchuck {
  public:
    WireLikeWiiNunchuck(WireLikeT &wire);

  protected:
    virtual int _read(uint8_t address, uint8_t data[], int length) override;
    virtual void _write(uint8_t address, uint8_t data) override;

  private:
    WireLikeT &_wire;
};

template<class WireLikeT>
WireLikeWiiNunchuck<WireLikeT>::WireLikeWiiNunchuck(WireLikeT &wire) : _wire(wire) {
  _wire.begin();
}

template<class WireLikeT>
int WireLikeWiiNunchuck<WireLikeT>::_read(uint8_t address, uint8_t data[], int length) {
  int l = 0;
  _wire.beginTransmission(0x52);
  _wire.write(address);
  _wire.endTransmission();
  delay(1);
  
  _wire.requestFrom(0x52, length);
  for (int i = 0; i < length && _wire.available(); i++) {
    data[i] = _wire.read();
    l++;
  }
  delay(1);
  return l;
}

template<class WireLikeT>
void WireLikeWiiNunchuck<WireLikeT>::_write(uint8_t address, uint8_t data) {
  _wire.beginTransmission(0x52);
  _wire.write(address);
  _wire.write(data);
  _wire.endTransmission();
  delay(1);
}

using WireWiiNunchuck = WireLikeWiiNunchuck<TwoWire>;

#endif // WIREWIINUNCHUCK

